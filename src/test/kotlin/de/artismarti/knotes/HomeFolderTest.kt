package de.artismarti.knotes

import org.junit.AfterClass
import org.junit.Test
import java.nio.file.Files


/**
 * @author artur
 */
class HomeFolderTest {

    companion object {
        @AfterClass
        @JvmStatic
        fun tearDown() {
            Files.deleteIfExists(HomeFolder.newOrGetFile("test.txt"))
        }
    }

    @Test
    fun testGet() {
        assert(Files.exists(HomeFolder.get()))
    }

    @Test
    fun testNewFile() {
        assert(Files.exists(HomeFolder.newOrGetFile("test.txt")))
    }
}
