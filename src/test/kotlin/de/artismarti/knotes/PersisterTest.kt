package de.artismarti.knotes

import de.artismarti.knotes.domain.Data
import de.artismarti.knotes.domain.Folder
import de.artismarti.knotes.domain.LongNote
import de.artismarti.knotes.domain.QuickNote
import org.junit.BeforeClass
import org.junit.Test
import java.nio.file.Files
import java.time.LocalDateTime
import java.time.Month

/**
 * @author artur
 */
class PersisterTest {

    private val persister = Persister()

    companion object {
        private val referenceData = Data("test", listOf(
                Folder("folder", listOf(
                        QuickNote("Message"),
                        LongNote("Message", ".....", "Home", "Category",
                                LocalDateTime.of(2016, Month.FEBRUARY, 17, 11, 30),
                                LocalDateTime.of(2016, Month.FEBRUARY, 17, 11, 30))))
        ))

        private val testData = """
        { "name" : "test",
            "folders" : [
                { "name" : "folder",
                "notes" : [
                    { "type" : "longNote",
                        "message" : "Message" },
                    { "type" : "quickNote",
                        "message" : "Message",
                        "description" : ".....",
                        "location" : "Home",
                        "category" : "Category",
                        "startTime" : [ 2016, 2, 17, 11, 30 ],
                        "endTime" : [ 2016, 2, 17, 11, 30 ] }
                    ]
                }
            ]
        }  """

        @BeforeClass
        @JvmStatic
        fun setUp() {
            Files.write(HomeFolder.newOrGetFile("test"), testData.toByteArray())
        }
    }

    @Test
    fun testSave() {
        assert(persister.save(referenceData))
    }

    @Test
    fun testLoad() {
        val data = persister.load("test")
        assert(data.equals(referenceData))
    }
}
