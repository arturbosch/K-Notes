package de.artismarti.knotes

import de.artismarti.knotes.domain.Folder
import de.artismarti.knotes.domain.QuickNote
import org.junit.Test

/**
 * @author artur
 */
class FolderTest {

    private val folder = Folder("name", listOf(QuickNote("first")))

    @Test
    fun testAdd() {
        folder.add(QuickNote("note"))
        assert(folder.notes.size == 2)
    }

    @Test
    fun testRemove() {
        folder.remove(0)
        assert(folder.notes.size == 0)
    }

    @Test
    fun testEdit() {
        folder.edit(0, QuickNote("edit"))
        assert(folder.notes.size == 1)
        assert(folder.notes[0].message == "edit")
    }
}
