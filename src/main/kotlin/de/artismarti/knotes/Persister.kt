package de.artismarti.knotes

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import de.artismarti.knotes.domain.Data
import java.nio.file.Files

/**
 * @author artur
 */
class Persister {

    private val objectMapper = ObjectMapper().apply {
        registerModule(JavaTimeModule())
    }
    private val dataType = Data::class.java

    fun save(data: Data): Boolean {
        val bytes = objectMapper
                .writerWithDefaultPrettyPrinter()
                .writeValueAsBytes(data)
        val savePlace = HomeFolder.newOrGetFile(data.name)
        val savedPath = Files.write(savePlace, bytes)
        return Files.exists(savedPath)
    }

    fun load(dataName: String): Data {
        val pathToData = HomeFolder.newOrGetFile(dataName)
        val loadedData = Files.readAllLines(pathToData).joinToString(separator = "")
        return objectMapper.readValue(loadedData, dataType)

    }
}
