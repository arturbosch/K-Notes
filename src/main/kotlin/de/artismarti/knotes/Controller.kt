package de.artismarti.knotes

import de.artismarti.knotes.strategy.DefaultControlStrategy

/**
 * @author artur
 */
class Controller(val out: ConsoleWriter, val dataManager: DataManager) {

    fun printMainMenu() {
        out.println("Welcome to K-Notes!")
        out.println("1: list notes, 2: add note, 3: update note, 4: delete note, 5: list folders 0: exit")
    }

    fun printChooseFolder() {
        out.println("Choose folder:")
        out.println(dataManager.listFolders())
    }

    fun printAddNote() {
        out.println("Choose message type:")
        out.println("1. Quick Note (just some text) ")
        out.print("2. Long Note (more information to store) ")
    }

    fun printRemoveNote(folderIndex: Int) {
        out.println("Choose which note to remove:")
        out.println(dataManager.listNotes(folderIndex))
    }

    fun printEditNote(folderIndex: Int) {
        out.println("Choose which note to edit:")
        out.println(dataManager.listNotes(folderIndex))
    }

    fun printHelpMenu() {
        clear {
            out.apply {
                clear(5)
                kotlin.io.println("Useful commands:")
                kotlin.io.println("? -> show commands")
                kotlin.io.println("! -> main menu")
                kotlin.io.println("0 -> exit K-Notes")
            }
        }
    }

    fun menu() {
        clear {
            printMainMenu()
        }
    }

    private inline fun clear(before: Int = 5, after: Int = 3, block: () -> Unit) {
        out.clear(before)
        block.invoke()
        out.clear(after)
    }

    fun listNotes(index: Int) {
        out.println(dataManager.listNotes(index))
    }

    fun listFolders() {
        out.println(dataManager.listFolders())
    }

    fun printEnterNote() {
        out.println("Enter a note to store:")
    }

    fun printAddedNewNote() {
        out.println("Added new note to selected folder.")
    }

    fun saveNewNote(protoNote: DefaultControlStrategy.ProtoNote) {
        dataManager.addNote(protoNote.folderIndex, protoNote.msg)
    }

    fun folderExists(index: Int): Boolean {
        return dataManager.countFolders(index + 1)
    }

    fun printNoSuchFolder(folderOrNote: String) {
        out.println("There is no $folderOrNote on this index.")
    }

    fun noteExists(folderIndex: Int, noteIndex: Int): Boolean {
        return dataManager.countNotes(folderIndex, noteIndex + 1)
    }

    fun removeOldNote(protoNote: DefaultControlStrategy.ProtoNote) {
        dataManager.removeNote(protoNote.folderIndex, protoNote.noteIndex)
    }

    fun printRemovedNote() {
        out.println("Removed note from selected folder.")
    }

    fun editOldNote(protoNote: DefaultControlStrategy.ProtoNote) {
        dataManager.editNote(protoNote.folderIndex, protoNote.noteIndex, protoNote.msg)
    }

    fun printEditNote() {
        out.println("Enter a message to replace the old:")
    }

    fun printEditedNote() {
        out.println("Edited note from selected folder.")
    }

}
