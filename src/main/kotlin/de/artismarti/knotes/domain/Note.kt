package de.artismarti.knotes.domain

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo

/**
 * @author artur
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes(
        JsonSubTypes.Type(value = LongNote::class, name = "quickNote"),
        JsonSubTypes.Type(value = QuickNote::class, name = "longNote")
)
abstract class Note(@JsonProperty("message") val message: String)
