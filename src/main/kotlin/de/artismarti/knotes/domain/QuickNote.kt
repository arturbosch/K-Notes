package de.artismarti.knotes.domain

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.Objects

/**
 * @author artur
 */
class QuickNote(@JsonProperty("message") message: String) : Note(message) {
    override fun toString(): String {
        return message
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as QuickNote

        if (message != other.message) return false

        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(message)
    }
}
