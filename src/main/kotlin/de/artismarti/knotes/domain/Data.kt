package de.artismarti.knotes.domain

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * @author artur
 */
data class Data(@JsonProperty("name") var name: String = "notes", @JsonProperty("folders") var folders: List<Folder> = listOf(Folder())) {
    override fun toString(): String {
        val sb = StringBuilder()
        for ((i, folder) in folders.withIndex()) {
            sb.append(i + 1).append(". ").append(folder.name).append("\n")
        }
        return sb.toString()
    }
}
