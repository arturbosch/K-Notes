package de.artismarti.knotes.domain

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime
import java.util.Objects

/**
 * @author artur
 */
class LongNote(@JsonProperty("message") message: String,
               @JsonProperty("description") val description: String,
               @JsonProperty("location") val location: String,
               @JsonProperty("category") val category: String,
               @JsonProperty("startTime") val startTime: LocalDateTime,
               @JsonProperty("endTime") val endTime: LocalDateTime) : Note(message) {

    override fun toString(): String {
        return "$message ${nlt()}" +
                "$category ${nlt()}" +
                "$location ${nlt()}" +
                "$startTime ${nlt()}" +
                "$endTime ${nlt()}" +
                "$description"
    }

    private fun nlt(): String = "\n\t"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as LongNote

        if (message != other.message) return false
        if (description != other.description) return false
        if (location != other.location) return false
        if (category != other.category) return false
        if (startTime != other.startTime) return false
        if (endTime != other.endTime) return false

        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(message, description, location, category, startTime, endTime)
    }
}
