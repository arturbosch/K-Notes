package de.artismarti.knotes.domain

import com.fasterxml.jackson.annotation.JsonProperty
import de.artismarti.knotes.util.replace

/**
 * @author artur
 */
data class Folder(@JsonProperty("name") var name: String = "myNotes",
                  @JsonProperty("notes") var notes: List<Note> = listOf()) {
    override fun toString(): String {
        val sb = StringBuilder()
        for ((i, note) in notes.withIndex()) {
            sb.append(i + 1).append(". ").append(note).append("\n")
        }
        return sb.toString()
    }

    fun add(note: Note) {
        notes = notes.plus(note)
    }

    fun remove(noteIndex: Int) {
        notes = notes.filterIndexed { i, note -> i != noteIndex}
    }

    fun edit(indexNote: Int, note: Note) {
        notes = notes.replace(indexNote, note)
    }
}
