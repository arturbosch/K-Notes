package de.artismarti.knotes

import de.artismarti.knotes.strategy.AbstractControlStrategy
import java.util.Scanner

/**
 * @author artur
 */
class ConsoleReader(val strategy: AbstractControlStrategy,
                    val scanner: Scanner) {

    fun start() {
        strategy.start()
        while (true) {
            read()
        }
    }

    private fun read() {
        val input = scanner.nextLine()
        strategy.evaluate(input)
    }

}

