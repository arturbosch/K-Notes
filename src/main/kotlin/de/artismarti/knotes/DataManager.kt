package de.artismarti.knotes

import de.artismarti.knotes.domain.Data
import de.artismarti.knotes.domain.QuickNote
import java.util.concurrent.CompletableFuture

/**
 * @author artur
 */
class DataManager(val persister: Persister, val saveName: String) {

    private lateinit var data: Data

    init {
        if (HomeFolder.fileExists(saveName)) {
            try {
                data = persister.load(saveName)
            } catch(ex: Exception) {
                Main.exitUnexpected("A problem occurred loading notes from file name: $saveName. Make sure it's exist and has it's content represented as json.", ex)
            }
        } else {
            data = Data()
        }
    }

    fun listNotes(folderIndex: Int): String {
        return data.folders[folderIndex].toString()
    }

    fun listFolders(): String {
        return data.toString()
    }

    fun addNote(folderIndex: Int, noteMessage: String) {
        data.folders[folderIndex].add(QuickNote(noteMessage))
        trySaveData()
    }

    private fun trySaveData() {
        CompletableFuture.runAsync {
            persister.save(data)
        }
    }

    fun removeNote(folderIndex: Int, noteIndex: Int) {
        data.folders[folderIndex].remove(noteIndex)
        trySaveData()
    }

    fun editNote(folderIndex: Int, indexNote: Int, noteMessage: String) {
        data.folders[folderIndex].edit(indexNote, QuickNote(noteMessage))
        trySaveData()
    }

    fun countFolders(size: Int): Boolean {
        return data.folders.size >= size
    }

    fun countNotes(folderIndex: Int, size: Int): Boolean {
        return data.folders[folderIndex].notes.size >= size
    }

}
