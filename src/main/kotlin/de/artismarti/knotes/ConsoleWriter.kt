package de.artismarti.knotes

import java.io.PrintStream

/**
 * @author artur
 */
class ConsoleWriter(val stream: PrintStream) {

    fun print(message: String = " ") {
        stream.print(message)
    }

    fun println(message: String = "") {
        stream.println(message)
    }

    fun clear(nls: Int) {
        for (i in 1..nls) println()
    }
}
