package de.artismarti.knotes

import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

/**
 * @author artur
 */
object HomeFolder {

    private val notesDir = "K-Notes"
    private val homeDir = System.getProperty("user.home") + File.separator + notesDir
    private val homePath = Paths.get(homeDir)

    fun get(): Path {
        if (Files.notExists(homePath))
            Files.createDirectory(homePath)
        return homePath
    }

    fun newOrGetFile(subPathInKNotesDir: String): Path {
        val newFile = homePath.resolve(subPathInKNotesDir)
        if (Files.notExists(newFile))
            Files.createFile(newFile)
        return newFile
    }

    fun fileExists(fileName: String): Boolean = Files.exists(homePath.resolve(fileName))
}
