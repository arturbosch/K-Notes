package de.artismarti.knotes.strategy

import de.artismarti.knotes.Controller
import de.artismarti.knotes.Main

/**
 * @author artur
 */
class DefaultControlStrategy(controller: Controller) : AbstractControlStrategy(controller) {

    enum class MODE {
        MENU, LIST, ADD, REMOVE, EDIT, WHICH_TYPE, WHICH_REMOVE, WHICH_EDIT, QUICK_TYPE_ADD, LONG_TYPE_ADD, QUICK_TYPE_EDIT, LONG_TYPE_EDIT
    }

    data class ProtoNote(var folderIndex: Int = -1, var msg: String = "", var noteIndex: Int = -1) {
        fun reset() {
            folderIndex = -1
            msg = ""
            noteIndex = -1
        }
    }

    private var mode = MODE.MENU
    private var protoNote = ProtoNote()

    override fun evaluate(input: String) {
        when (mode) {
            MODE.MENU -> menuChoices(input)
            MODE.LIST -> folderOrNoteChoices(input)
            MODE.ADD -> folderOrNoteChoices(input)
            MODE.WHICH_TYPE -> typeChoices(input)
            MODE.QUICK_TYPE_ADD, MODE.LONG_TYPE_ADD -> saveNewQuickNote(input)
            MODE.REMOVE -> folderOrNoteChoices(input)
            MODE.WHICH_REMOVE -> folderOrNoteChoices(input)
            MODE.EDIT -> folderOrNoteChoices(input)
            MODE.WHICH_EDIT -> folderOrNoteChoices(input)
            MODE.QUICK_TYPE_EDIT, MODE.LONG_TYPE_EDIT -> saveEditedQuickNote(input)
        }
    }

    private fun saveEditedQuickNote(input: String) {
        protoNote.msg = input
        controller.editOldNote(protoNote)
        controller.printEditedNote()
        setMenuMode()
    }

    private fun saveNewQuickNote(input: String) {
        protoNote.msg = input
        controller.saveNewNote(protoNote)
        controller.printAddedNewNote()
        setMenuMode()
    }

    private fun typeChoices(input: String) {
        when (input) {
            "1" -> controller.printEnterNote().let { mode = MODE.QUICK_TYPE_ADD }
            "2" -> controller.printEnterNote().let { mode = MODE.QUICK_TYPE_ADD }
            "0" -> setAddMode()
        }
    }

    private fun folderOrNoteChoices(input: String) {
        try {
            val index = Integer.parseInt(input)
            if (index >= 1) {
                val folderOrNoteIndex = index - 1
                if (isListOrAddOrEditOrRemoveMode()) {
                    chosenFolder(folderOrNoteIndex)
                } else if (mode == MODE.WHICH_REMOVE) {
                    nodeRemoveChoices(folderOrNoteIndex)
                } else if (mode == MODE.WHICH_EDIT) {
                    nodeEditChoices(folderOrNoteIndex)
                }
            } else if (index == 0) {
                if (mode == MODE.WHICH_REMOVE) {
                    setRemoveMode()
                } else if (mode == MODE.WHICH_EDIT) {
                    setEditMode()
                } else {
                    setMenuMode()
                }
            }
        } catch(ex: NumberFormatException) {
            Main.error("Invalid value. Try a number.")
        }
    }

    private fun isListOrAddOrEditOrRemoveMode() = mode == MODE.ADD || mode == MODE.REMOVE || mode == MODE.EDIT || mode == MODE.LIST

    private fun chosenFolder(index: Int) {
        if (controller.folderExists(index)) {
            if (mode == MODE.ADD) {
                chosenFolderForAdd(index)
            } else if (mode == MODE.REMOVE) {
                chosenFolderForRemove(index)
            } else if (mode == MODE.EDIT) {
                chosenFolderForEdit(index)
            } else if (mode == MODE.LIST) {
                chosenFolderForList(index)
            }
        } else {
            controller.printNoSuchFolder("folder")
        }
    }

    private fun chosenFolderForList(folderIndex: Int) {
        controller.listNotes(folderIndex)
        mode = MODE.MENU
    }

    private fun chosenFolderForAdd(index: Int) {
        mode = MODE.WHICH_TYPE
        controller.printAddNote()
        protoNote.folderIndex = index
    }

    private fun chosenFolderForEdit(index: Int) {
        mode = MODE.WHICH_EDIT
        controller.printEditNote(index)
        protoNote.folderIndex = index
    }

    private fun chosenFolderForRemove(index: Int) {
        mode = MODE.WHICH_REMOVE
        controller.printRemoveNote(index)
        protoNote.folderIndex = index
    }

    private fun nodeRemoveChoices(noteIndex: Int) {
        if (controller.noteExists(protoNote.folderIndex, noteIndex)) {
            protoNote.noteIndex = noteIndex
            controller.removeOldNote(protoNote)
            controller.printRemovedNote()
            setMenuMode()
        } else {
            controller.printNoSuchFolder("note")
        }
    }

    private fun nodeEditChoices(noteIndex: Int) {
        if (controller.noteExists(protoNote.folderIndex, noteIndex)) {
            protoNote.noteIndex = noteIndex
            controller.printEditNote()
            mode = MODE.QUICK_TYPE_EDIT
        } else {
            controller.printNoSuchFolder("note")
        }
    }

    private fun menuChoices(input: String) {
        when (input) {
            "1" -> setListMode()
            "2" -> setAddMode()
            "3" -> setEditMode()
            "4" -> setRemoveMode()
            "5" -> listFolders()
            "?" -> helpMenu()
            "!" -> setMenuMode()
            "0" -> Main.exit()
        }
    }

    private fun setListMode() {
        mode = MODE.LIST
        controller.printChooseFolder()
    }

    private fun helpMenu() {
        mode = MODE.MENU
        controller.printHelpMenu()
    }

    private fun listFolders() {
        mode = MODE.MENU
        controller.listFolders()
    }

    private fun setEditMode() {
        mode = MODE.EDIT
        chooseFolderAndResetProtoNote()
    }

    private fun setRemoveMode() {
        mode = MODE.REMOVE
        chooseFolderAndResetProtoNote()
    }

    private fun setMenuMode() {
        mode = MODE.MENU
        controller.menu()
    }

    private fun setAddMode() {
        mode = MODE.ADD
        chooseFolderAndResetProtoNote()
    }

    private fun chooseFolderAndResetProtoNote() {
        controller.printChooseFolder()
        protoNote.reset()
    }
}

