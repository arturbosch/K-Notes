package de.artismarti.knotes.strategy

import de.artismarti.knotes.Controller

/**
 * @author artur
 */
abstract class AbstractControlStrategy(val controller: Controller) {
    abstract fun evaluate(input: String)

    fun start() {
        controller.printMainMenu()
    }
}
