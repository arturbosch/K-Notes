package de.artismarti.knotes.util

/**
 * @author artur
 */
fun <E> List<E>.replace(indexNote: Int, note: E): List<E> {
    val list = this.toMutableList()
    list.removeAt(indexNote)
    list.add(indexNote, note)
    return list.toList()
}

