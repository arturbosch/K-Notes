package de.artismarti.knotes

import de.artismarti.knotes.strategy.DefaultControlStrategy
import java.util.Scanner

/**
 * @author artur
 */
object Main {

    private const val DEBUG = true

    @JvmStatic
    fun main(args: Array<String>) {

        val controller = Controller(ConsoleWriter(System.out), DataManager(Persister(), "data"))
        val strategy = DefaultControlStrategy(controller)
        val inputStream = Scanner(System.`in`)

        ConsoleReader(strategy, inputStream)
                .start()
    }

    fun error(msg: String) = println(msg)
    fun exit() = System.exit(0)
    fun exitUnexpected(msg: String, ex: Exception) {
        println("K-notes terminated unexpectedly with message: $msg!\n")
        if (DEBUG) println(ex)
        System.exit(1)
    }
}
