# K-Notes

Update: It was my first little kotlin playground, deprecated in favor of TiNBo using Spring Shell.

Command line tool to manage your notes and todos, created with Kotlin and with 
the goal to work perfectly with i3 window manager.
Command line tool to manage your notes and todos, created with Kotlin and 
the goal to work smoothly in combination with i3 window manager.

## Features
- navigate with keys: 1-9, 0, ?, !
- create, edit and remove notes/todos
- store them in different 'folders'

## Ideas
- turn on/off pretty printing
- two modes:
    - accept input immediately (done)
    - wait for user press enter enabling inputs like 211 (Add->Folder1->QuickNote)

## Changelog
### v0.1
- initial 'stable' version

## Installation
- clone project 
- cd project
- 'gradle fatjar' to build it
- java -jar K-Notes-[insert Version here].jar
- optional write a shell script for fast access 
